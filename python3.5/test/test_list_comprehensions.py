import unittest
from ddt import ddt, data
from list_comprehensions import ListComprehension as LC

@ddt
class TestListComprehensions(unittest.TestCase):

    testdata0 = (1, 1, 1, 2,
                 [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])

    testdata2 = (1, 2, 3, 3,
                 [[0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 1, 0], [0, 1, 1], [0, 1, 3], [0, 2, 0], [0, 2, 2], [0, 2, 3],
                  [1, 0, 0], [1, 0, 1], [1, 0, 3], [1, 1, 0], [1, 1, 2], [1, 1, 3], [1, 2, 1], [1, 2, 2], [1, 2, 3]])

    @data(testdata0)
    def test_one(self, value):
        self.assertListEqual(LC.make_mylist(value[0],value[1],value[2],value[3]), value[4])

    @data(testdata2)
    def test_one(self, value):
        print(LC.make_mylist(value[0],value[1],value[2],value[3]))
        print(value[4])
        self.assertListEqual(LC.make_mylist(value[0],value[1],value[2],value[3]), value[4])


if __name__ == '__main__':
    unittest.main()
