package greatXOR;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        long[] aa = new long[q];
        HashMap<Long, Long> cache = new HashMap<>(q);
        for(int a0 = 0; a0 < q; a0++) {
            long x = in.nextLong();
            aa[a0] = x;
        }
        long matches;
        for (long x : aa){

            if (cache.containsKey(x)){
                matches = cache.get(x);
            } else {
                matches = 0;
                for (long a=0; a < x; a++){
                    if ((a ^ x) > x) matches ++;
                }
                cache.put(x, matches);
            }
            System.out.println(matches);
        }
    }
}