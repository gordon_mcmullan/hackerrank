package interQuartileRange;

import java.util.*;

public class Solution {

    public static boolean isOdd(int number){
        if (number % 2 == 0){
            return false;
        } else {
            return true;
        }
    }

    public static Integer[] readArray(int count, Scanner scan) {
        Integer[] inputArray = new Integer[count];
        for (int i = 0; i < count; i++) {
            inputArray[i] = scan.nextInt();
        }
        return inputArray;

    }

    public static Integer[] fillArray(Integer[] digits, Integer[] frequency){
        ArrayList<Integer> filledArray = new ArrayList<Integer>();
        for (int i=0; i< digits.length ; i++){
            for(int j=0; j< frequency[i]; j++){
                filledArray.add(digits[i]);
            }
        }
        return filledArray.toArray(new Integer[filledArray.size()]);
    }

    public static int median (Integer[] integerArray){
        int median;
        if (isOdd(integerArray.length) == true){
            int medianPosition = (int)Math.floor(integerArray.length/2);
            median = integerArray[medianPosition];
        } else {
            median = (integerArray[integerArray.length/2-1] + integerArray[integerArray.length/2]) / 2;
        }
        //System.out.printf("median of %s is %d\n", Arrays.toString(integerArray), median);
        return median;
    }

    public static double[] calculateQuartiles(Integer[] integerArray){
        Arrays.sort(integerArray);
        double[] quartiles = new double[3];

        quartiles[1] = median(integerArray);
        quartiles[0] = median(Arrays.copyOf(integerArray, (int)Math.floor(integerArray.length/2)));
        quartiles[2] = median(Arrays.copyOfRange(integerArray, (int)Math.floor(integerArray.length/2 + integerArray.length%2), integerArray.length));
        return quartiles;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        Integer[] digits = readArray(count, scan); //new int[count];
        Integer[] frequency = readArray(count, scan);

        Integer[] values = fillArray(digits, frequency);

        double[] quartiles = calculateQuartiles(values);

        double interQuartileRange = quartiles[2] - quartiles[0];
        System.out.println(interQuartileRange);

    }
}