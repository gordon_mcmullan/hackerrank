package sockMerchant;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int c[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }

        HashMap<Integer, Integer> socksByColour = new HashMap<>();
        HashMap<Integer, Integer> sockPairsByColour = new HashMap<>();
        for (int i=0; i< c.length; i++) {
            if (socksByColour.containsKey(c[i])) {
                socksByColour.put(c[i], socksByColour.get(c[i]) + 1);
            } else {
                socksByColour.put(c[i], 1);
            }
            sockPairsByColour.put(c[i], (int) Math.floor(socksByColour.get(c[i]) / 2));
        }
        int totalPairs = 0;
        for (int pairs : sockPairsByColour.values()){
            totalPairs += pairs;
        }
        System.out.println(totalPairs);

    }
}