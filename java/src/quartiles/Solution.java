package quartiles;

import java.util.*;

public class Solution {

    public static boolean isOdd(int number){
        if (number % 2 == 0){
            return false;
        } else {
            return true;
        }
    }

    public static int median (int[] integerArray){
        int median;
        if (isOdd(integerArray.length) == true){
            int medianPosition = (int)Math.floor(integerArray.length/2);
            median = integerArray[medianPosition];
        } else {
            median = (integerArray[integerArray.length/2-1] + integerArray[integerArray.length/2]) / 2;
        }
        System.out.printf("median of %s is %d\n", Arrays.toString(integerArray), median);
        return median;
    }

    public static int[] calculateQuartiles(int[] integerArray){
        Arrays.sort(integerArray);
        int[] quartiles = new int[3];

        quartiles[1] = median(integerArray);
        quartiles[0] = median(Arrays.copyOf(integerArray, (int)Math.floor(integerArray.length/2)));
        quartiles[2] = median(Arrays.copyOfRange(integerArray, (int)Math.floor(integerArray.length/2), integerArray.length));
        return quartiles;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] values = new int[count];
        int[] quartiles = new int[3];

        for (int i=0; i < count; i++){
            values[i] = scan.nextInt();
        }


    quartiles = calculateQuartiles(values);
        System.out.println(quartiles[0]);
        System.out.println(quartiles[1]);
        System.out.println(quartiles[2]);

        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    }
}