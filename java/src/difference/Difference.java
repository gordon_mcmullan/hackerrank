package difference;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


class Difference {
    private int[] elements;
    public int maximumDifference;

    public Difference(int[] elements) {
        this.elements = elements;
    }

    protected void computeDifference() {
        System.out.println(this.elements.length);
        int maxValue = this.elements[0];
        int minValue = this.elements[0];
        for (int i : this.elements) {
            maxValue = Math.max(maxValue, i);
            minValue = Math.min(minValue, i);
        }
        this.maximumDifference = maxValue - minValue;

    } // End of Difference class
}