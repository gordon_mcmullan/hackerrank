package Averages;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] arr = new int[count];
        for(int i=0; i < count; i++){
            arr[i] = scan.nextInt();
        }
        System.out.printf("%.1f\n", mean(arr));
        System.out.printf("%.1f\n",median(arr));
        System.out.printf("%1d\n",mode(arr));
    }

    public static double mean(int[] arr) {
        int sum = 0;
        for (int i : arr){
            sum += i;
        }
        return sum / arr[arr.length-1];
    }

    public static double median(int[] arr) {
        Arrays.sort(arr);
        double median;
        if (arr.length % 2 == 0){
            median = ((double)arr[(arr.length/2)] + (double)arr[(arr.length/2)+1]) / 2.0;
        } else {
            median = (double)arr[(int)Math.ceil((arr.length)/2)];
        }
        return median;
    }


    public static int mode(int[] arr){
        Arrays.sort(arr);
        int maxSequence = 0;
        int mode = arr[0];
        for (int i=0; i < arr.length-2; i++) {
            int sequence = 0;
            if (arr[i] == arr[i+1]) {
                sequence++;
                if (sequence > maxSequence) {
                    maxSequence = sequence;
                    mode = arr[i];
                }
            } else {
                sequence = 0;
            }
        }
        return mode;
    }
}