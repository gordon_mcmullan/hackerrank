package staircase;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    /*
 * Complete the function below.
 */

    static void StairCase(int n){
        for (int i=n; i > 0; i--) {
            for (int j=0; j < i-1; j++){
                System.out.print(" ");
            }
            for (int j=0; j < n-i; j++) {
                System.out.print("#");
            }
            System.out.print("\n");
        }
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int _n;
        _n = Integer.parseInt(in.nextLine().trim());

        StairCase(_n);

    }
}