package timemanipulation;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String timeString = in.next();

        boolean pm;
        String[] timeArray = new String[3];
        String[] tmp = timeString.split(":");
        for (int i = 0; i < 3; i++) {
            timeArray[i] = tmp[i];
        }
        if(tmp[2].contains("PM") && Integer.parseInt(timeArray[0]) < 12) {
            timeArray[0] = Integer.toString(Integer.parseInt(timeArray[0]) + 12 );
        } else if (tmp[2].contains("AM") && Integer.parseInt(timeArray[0]) == 12) {
            timeArray[0] = "00";
        }
        timeArray[2] = timeArray[2].substring(0,2);

        System.out.printf("%s:%s:%s", timeArray[0], timeArray[1], timeArray[2] );



    }
}