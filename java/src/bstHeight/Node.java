package bstHeight;

/**
 * Created by Gordon McMullan on 13/01/2017.
 */
import java.util.*;
import java.io.*;
class Node{
    Node left,right;
    int data;
    Node(int data){
        this.data=data;
        left=right=null;
    }
}

class Solution{

    public static int getHeight(Node root){
        //Write your code here
        Node node = root;
        int height;
        if (node.left !=null && node.right != null){
            height = 1 + Math.max(getHeight(node.left), getHeight(node.right));
        } else if (node.left !=null) {
            height = 1 + getHeight(node.left);
        } else if (node.right != null) {
            height = 1 + getHeight(node.right);
        } else {
            height = 0;
        }
        return height;
    }

    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root=insert(root,data);
        }
        int height=getHeight(root);
        System.out.println(height);
    }
}
