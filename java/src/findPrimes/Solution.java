package findPrimes;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        for(int i=0; i < count; i++){
            int pp = scan.nextInt();
            if (isPrime(pp)) {
                System.out.println("Prime");
            } else {
                System.out.println("Not prime");
            }
        }
    }

    public static boolean isPrime(int i) {
        // deal with special cases
        switch (i){
            // Special cases
            case 1:
                return false;
            default:
                return doCalculation(i);
        }

    }

    public static boolean doCalculation(int i){

        if (i % 2 == 0){
            return false;
        } else if (i % 3 == 0){
            return false;
        } else if (i % 6 == 1 || i % 6 == 5 ){
            for (int j = 2; j < Math.sqrt(i); i++) {
                if (i % j == 0) {
                    return true;
                }
            }
            return false;
        } else{
            return false;
        }
    }

}