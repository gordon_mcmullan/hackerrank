package weightedMean;

/**
 * Created by Gordon McMullan on 15/01/2017.
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int count = scan.nextInt();
        int[] values = new int[count];
        int[] weights = new int[count];
        for(int i=0; i < count; i++){
            values[i] = scan.nextInt();
        }
        for(int i=0; i < count; i++){
            weights[i] = scan.nextInt();
        }
        double dividend = 0;
        double divisor = 0;

        for (int i=0; i < count; i++){
            dividend += values[i] * weights[i];
            divisor += weights[i];
        }
        System.out.printf("%.1f\n",(dividend / divisor));

    }
}