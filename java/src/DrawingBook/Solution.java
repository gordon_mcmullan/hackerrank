package DrawingBook;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int p = in.nextInt();
        // your code goes here
        int spreads = (n / 2);
        int targetSpread = (p / 2);
        System.out.print(Math.min(targetSpread, spreads - targetSpread));


    }
}