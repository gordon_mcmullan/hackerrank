package Sorting1;

/**
 * Created by Gordon McMullan on 05/01/2017.
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int value = scan.nextInt();
        int count = scan.nextInt();
        int[] arr = new int[count];
        for (int i=0; i < count; i++){
            arr[i] = scan.nextInt();
        }

        for(int i=0; i < arr.length; i++){
            if (arr[i] == value){
                System.out.println(i);
            }

        }

    }

}
