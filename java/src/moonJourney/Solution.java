package moonJourney;
import java.io.*;
import java.util.*;

class Countries{

    private ArrayList<HashSet<Integer>> astronauts;
    private HashMap<Integer, Integer> astronautIndex;
    private int initialSize;
    Countries() {
        astronauts = new ArrayList<HashSet<Integer>>();
    }

    Countries(int initialSize) {
        astronauts = new ArrayList<HashSet<Integer>>(initialSize);
        astronautIndex = new HashMap<Integer, Integer> (initialSize);
        this.initialSize = initialSize;
    }

    void pairAstronauts(Integer astronaut, Integer pair) {

        if (astronautIndex.containsKey(astronaut) && astronautIndex.containsKey(pair)){
            int ia = astronautIndex.get(astronaut);
            int ip = astronautIndex.get(pair);
            if (ia != ip) { // not already paired
                astronauts.get(ia).addAll(astronauts.get(ip));
                for (Integer a : astronauts.get(ip)) {
                    astronautIndex.put(a, ia);
                }
                //astronautIndex.put(pair, ia);
                astronauts.set(ip, null);
             }
             // above skipped if already paired

        } else if (astronautIndex.containsKey(astronaut)){
            int i = astronautIndex.get(astronaut);
            astronauts.get(i).add(pair);
            astronautIndex.put(pair, i);
            //assert(i!=237);

        } else if (astronautIndex.containsKey(pair)){
            int i = astronautIndex.get(pair);
            astronauts.get(i).add(astronaut);
            astronautIndex.put(astronaut, i);
            //assert(i!=237);;

        } else {
            //we should only get here for new astronauts
            HashSet<Integer> a = new HashSet<Integer>();
            a.add(astronaut);
            a.add(pair);
            int newIndex = astronauts.size();
            astronauts.add(a);
            astronautIndex.put(astronaut,newIndex);
            astronautIndex.put(pair, newIndex);
            //assert(newIndex!=237);
        }
        return;
    }

    void assignLoneAstronautToNewCountry(Integer number){
        int newIndex = astronauts.size();
        HashSet<Integer> a = new HashSet<Integer>(1);
        a.add(number);
        astronauts.add(a);
        astronautIndex.put(number, newIndex);

        return;
    }

    ArrayList<HashSet<Integer>> getAstronauts(){
        ArrayList<HashSet<Integer>> allNonNullCountries = new ArrayList<HashSet<Integer>>(initialSize);
        //astronauts.trimToSize();
        for (int i=0; i < astronauts.size();  i++){
            if (astronauts.get(i) != null){
                allNonNullCountries.add(astronauts.get(i));
            }
        }
        return allNonNullCountries;
    }

    int countryCount(){
        return astronauts.size();
    }
}

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));

        String[] temp = bfr.readLine().split(" ");
        int N = Integer.parseInt(temp[0]);
        int I = Integer.parseInt(temp[1]);

        long combinations = 0;
        Countries countries = new Countries(I);

        HashSet<Integer> unPairedAstronauts = new HashSet<Integer>(N);
        for(int i=0; i < N; i++){
            unPairedAstronauts.add(i);
        }

        for(int i = 0; i < I; i++){
            temp = bfr.readLine().split(" ");
            int a = Integer.parseInt(temp[0]);
            int b = Integer.parseInt(temp[1]);
            // Store a and b in an appropriate data structure of your choice
            countries.pairAstronauts(a, b);
            unPairedAstronauts.remove(a);
            unPairedAstronauts.remove(b);
        }

        //for (int astronaut : unassignedAstronauts){
           // countries.assignLoneAstronautToNewCountry(astronaut);
        //}
        int unAssignedAstronauts = N;
        ArrayList<HashSet<Integer>> assignedAstronauts = countries.getAstronauts();

        for (int i = 0; i <assignedAstronauts.size() ; i++) {
            int nauts = assignedAstronauts.get(i).size();
            unAssignedAstronauts = unAssignedAstronauts - nauts;
            combinations += nauts * unAssignedAstronauts;

        }
        combinations += unPairedAstronauts.size() * (unPairedAstronauts.size() - 1L)/2L;

        // Compute the final answer - the number of combinations
        System.out.println(combinations);

    }
}