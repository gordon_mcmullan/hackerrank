package regex1;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        ArrayList<String> gmailUsers = new ArrayList<String>();

        for(int a0 = 0; a0 < N; a0++){
            String firstName = in.next();
            String emailID = in.next();

            Pattern gmail = Pattern.compile("\\@gmail\\.com");
            if (gmail.matcher(emailID).find()){
                gmailUsers.add(firstName);
            }
        }
        Collections.sort(gmailUsers);
        for(String user : gmailUsers){
            System.out.println(user);
        }

     }
}
