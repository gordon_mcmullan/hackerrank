package classCancellation;
import java.util.*;
/**
 * Created by Gordon McMullan on 17/01/2017.
 */
public class otherSolution {

    static int rnGen(int min, int max){
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static void main(String[] args) {
        System.out.println("5");
        for(int i = 0; i < 5; i++){
            int no_stud = rnGen(1, 10);
            int lect_thresh = rnGen(1, no_stud);
            StringBuilder sb = new StringBuilder(no_stud);
            for(int j = 1; j < no_stud; j++){
                sb.append(rnGen(-1000, 1000) + " ");
            }
            sb.append(0);
            System.out.println(no_stud + " " + lect_thresh);
            System.out.println(sb);

        }

    }
}