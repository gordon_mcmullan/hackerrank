package classCancellation;

/**
 * Created by Gordon McMullan on 17/01/2017.
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static int testCases = 5;
    public static boolean[] outcomes = {true, false, true, false, true}; // is a class cancelled

    public static void main(String[] args) {



        System.out.println(testCases);
        for (int i=0; i < testCases; i++) {
            int studentCount = 3 + i;
            boolean outcome = outcomes[i];

            System.out.print(studentCount + " ");
            System.out.println(outcome? studentCount/2 +2 : studentCount/2 );
            StringBuilder sb = new StringBuilder();
            sb.append("-1 0 1");

            for (int j = 2; j < studentCount; j++) {
                sb.append(outcome? " " + -j : " " + j);
            }
            System.out.println(sb.toString());


        }


    }
}

