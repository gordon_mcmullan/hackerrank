package pangram;

/**
 * Created by Gordon McMullan on 12/01/2017.
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        String testString = scan.nextLine();
        Set<Character> alphabet = new HashSet<Character>();
        for(int i=0; i< 26; i++){
            alphabet.add(new Character(Character.toChars("a".charAt(0)+i)[0]));
        }
        Set<Character> testChars = new HashSet<Character>();
        for (char c : testString.toLowerCase().toCharArray()){
            testChars.add(c);
        }
        testChars.retainAll(alphabet);
        if (testChars.equals(alphabet)) {
            System.out.println("pangram");
        } else {
            System.out.println("not pangram");
        }

    }
}
